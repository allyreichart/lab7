/*Lab6.cpp
***********************************
Ally Reichart- amreich
TAs- Nushrat Humaira and John Choi
CPSC 1021-001
Lab 6
Submitted on: 9/30/20
************************************
The purpose of this program is to create a program using c++ that utilizes a struct
containg information about employees, then sorting the data randomly using functions provided in
specific c++ libraries. From this a second array was created, containing
the first five structs from the first array and sorted in alphabetical
order by last name and the results were printed to the screen.


Academic Honest Declaration:
  The following code represents my own work and I have neither received not
  given assistance that violates the collaboration policy posted with this
  assignment. I have not copied or modified code from any other source other
  than the lab assignment, course textbook, or course lecture slides. Any
  unauthorized collaboration or use of materials not permitted will be
  subjected to academic integrity policies of Clemson University and CPSC
  1020/1021.

  I acknowledge that this lab assignment is based upon an assignment created by
  Clemson University and that any publishing or posting of this code is
  prohibited unless I receive written permission from Clemson University.
  */


#include <iostream>
#include <iomanip>
#include <algorithm>
#include <sstream>
#include <string>
#include <cstdlib>

using namespace std;


typedef struct Employee{
	string lastName;
	string firstName;
	int birthYear;
	double hourlyWage;
}employee;

bool name_order(const employee& lhs, const employee& rhs);
int myrandom (int i) { return rand()%i;}


int main(int argc, char const *argv[]) {
  srand(unsigned (time(0))); //used to generate random seed



 	employee emparr[10]; //array of structs to hold 10 employees data
	int i;

	//prompts to insert employee data
	for(i = 0; i<10; i++) {
		//iterates through 10 times to allow user to input data for 10 employees
		cout << setw(10) << "Employee" << "[" << i+1 << "]" << "\n"; //displays which employee to be inputting data for
		cout << "Enter last name:";
		cin >> emparr[i].lastName;
		cout << "Enter first name:";
		cin >> emparr[i].firstName;
		cout << "Enter birth year:";
		cin >> emparr[i].birthYear;
		cout << "Enter hourly wage:";
		cin >> emparr[i].hourlyWage;
		cout << endl;
	}


	 random_shuffle(begin(emparr), end(emparr), myrandom);
	 //randomly shuffles the employee array using the myrandom and srand funtion as a seed value


		employee emparr2[5]; //declaring a second array of 5 structs
		for (i = 0; i < 5; i++) {
			//iterates through the first 5 values of the initial array and sets those values to the corresponding places in the new array
			emparr2[i] = emparr[i];
		}

    /*Sort the new array.  Links to how to call this function is in the specs
     *provided*/
		 sort(begin(emparr2), end(emparr2), name_order);
		 //sorts the new array  by last name

    /*Now print the array below */
		cout << "Sorted Employee Data" << "\n" << endl;

		for (auto i: emparr2) {
			//displays the new array data by employee, right aligned
			cout << setw(20) << right << i.lastName+", " +i.firstName << endl;
			cout << setw(20) << i.birthYear << endl;
			cout << setw(20) << fixed << setprecision(2) << showpoint << i.hourlyWage << endl;
			//precision set to 2 for 2 places after the decimal
		}


  return 0;
}


bool name_order(const employee& lhs, const employee& rhs) {
	//compares the ANSI values of the letters and returns true if the left hand side is less than the right
	if (lhs.lastName < rhs.lastName)  {
		return true;
	}
	else {
		return false;
	}
}
